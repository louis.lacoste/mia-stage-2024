---
format: html
title: Clustering avec `colSBM` des données Doré et al. seulement sur les larges réseaux
execute: 
  echo: false
  warning: false
---


# Analyse


```{r}
library(colSBM)
library(here)
library(dplyr)
library(stringr)
```

```{r}
root_app_folder <- file.path(here(), "code", "applications")
source(file.path(root_app_folder, "utils.R"))
data_folder <- file.path(here(), "code", "results", "applications", "dore_no_small")

# Full data
list_full_files <- file.path(
    data_folder,
    list.files(data_folder,
        pattern = "full"
    )
)
names(list_full_files) <- stringr::str_extract(
    string = list_full_files,
    pattern = "(iid|pirho|pi|rho)"
)

lapply(seq_len(length(list_full_files)), function(idx) {
    min_size <- readRDS(list_full_files[[idx]])[[1]]
    clustering <- readRDS(list_full_files[[idx]])[[2]]

    min_filepath <- gsub("full_", "min_size_", list_full_files[[idx]])
    clustering_filepath <- gsub("full_", "clustering_", list_full_files[[idx]])

    saveRDS(min_size, min_filepath)
    saveRDS(clustering, clustering_filepath)
})

```

```{r}
#| output: false
files_vec <- get_recent_files(data_folder, pattern = "clustering")
min_vec <- get_recent_files(data_folder, pattern = "min")
files_vec <- identify_models(files_vec)
min_vec <- identify_models(min_vec)

# Importing data
base_data_folder <- file.path(here(), "code", "data", "dore")

interaction_data <- read.table(file = file.path(base_data_folder, "interaction-data.txt"), sep = "\t", header = TRUE)

seq_ids_network_aggreg <- unique(interaction_data$id_network_aggreg)
names_aggreg_networks <- sapply(
    seq_ids_network_aggreg,
    function(id) {
        paste0(
            unique(interaction_data[which(interaction_data$id_network_aggreg == id), ]$web),
            collapse = "+"
        )
    }
)
# Computation of incidence matrices
incidence_matrices <- lapply(
    seq_ids_network_aggreg,
    function(m) {
        current_interaction_data <- interaction_data[which(interaction_data$id_network_aggreg == m), ] %>%
            mutate(
                plantaggreg = paste(plantorder,
                    plantfamily, plantgenus, plantspecies,
                    sep = "-"
                ),
                insectaggreg = paste(insectorder,
                    insectfamily, insectgenus, insectspecies,
                    sep = "-"
                )
            )
        current_interaction_data <- table(current_interaction_data$plantaggreg, current_interaction_data$insectaggreg)

        current_incidence_matrix <- matrix(current_interaction_data,
            ncol = ncol(current_interaction_data), dimnames = dimnames(current_interaction_data)
        )

        current_incidence_matrix[which(current_incidence_matrix > 0)] <- 1
        return(current_incidence_matrix)
    }
)

names(incidence_matrices) <- names_aggreg_networks

min_sizes <- lapply(min_vec, function(file) readRDS(file))
nb_networks <- sapply(min_sizes, function(min_size) {
    length(incidence_matrices[
        (colSums(sapply(incidence_matrices, dim) > min_size) == 2L)
    ])
})
min_sizes_df <- do.call(rbind.data.frame, min_sizes)
min_sizes_df <- cbind(min_sizes_df, nb_networks)
colnames(min_sizes_df) <- c("nr >", "nc >", "nb_networks")

list_clustering <- files_vec
# list_collection <- readRDS(file.path(data_folder, "dore_collection_iid_24-05-24_18-07-50.Rds"))
```


Les données ont été filtrées pour retenir les réseaux de taille supérieure à 
celles du tableau ci-dessous, aboutissant au nombre de réseaux de la colonne de
droite :
`r knitr::kable(min_sizes_df, caption = "Table des tailles minimales retenues par modèle")`

## Analyse par modèle

Les clustering donne le critère suivant :

```{r}
vec_bicl <- sapply(list_clustering, function(clustering) {
    list_collection <- readRDS(clustering)
    unlisted_best_partition <- unlist(
        extract_best_bipartite_partition(list_collection)
    )
    BICL <- sum(sapply(unlisted_best_partition, function(col) col$BICL))
    BICL
})
knitr::kable(vec_bicl, caption = "BICL par modèle", col.names = "$BICL$", row.names = TRUE)
```


:::{.panel-tabset}


```{r write_tabs}
#| warning: false
#| output: asis

# Generate content for each model using knit_expand
for (clustering_idx in seq_len(length(list_clustering))) {
    clustering <- list_clustering[clustering_idx]
    model <- names(clustering)
    expanded_content <- knitr::knit_expand(file = file.path(root_app_folder, "base_analysis.qmd"), clustering = clustering, model = model)
    res <- knitr::knit_child(text = expanded_content, quiet = TRUE)
    cat(res, sep = "\n")
    cat("\n")
}
```


:::
