library(here)

data_folder <- file.path(here(), "code", "data", "weboflife")
data_path <- file.path(data_folder, "web_of_life.rda")
load(data_path)

netlist <- lapply(web_of_life, function(web) web[["net"]])
names(netlist) <- sapply(web_of_life, function(web) web[["id"]])
