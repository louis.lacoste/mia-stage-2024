### Modèle  ${{model}}$

```{r}
library(here)

library(colSBM)

library(dplyr)
library(tidyr)

library(plotly)
library(ggplot2)
library(ggforce)
library(ggrepel)
# library(ggtree) # devtools::install_github("YuLab-SMU/ggtree")
library(ggdendro)
library(phylogram)
library(plotly)
library(latex2exp)
```
```{r}
list_collection <- readRDS("{{clustering}}")
unlisted_best_partition <- extract_best_partition(list_collection)

if (is.list(unlisted_best_partition)) {
    BICL <- sum(sapply(
        unlisted_best_partition,
        function(col) col[["BICL"]]
    ))
} else {
    BICL <- unlisted_best_partition[["BICL"]]
    unlisted_best_partition <- list(unlisted_best_partition)
}
```

Le clustering donne le critère suivant : $BICL = `r BICL`$

```{r}
graph_size_df <- build_graph_size_dataframe(unlisted_best_partition)
summarized_graph_size_df <- graph_size_df %>%
    group_by(collection_id) %>%
    select(-net_id) %>%
    summarise(
        M = n(),
        nr_mean = mean(nr),
        nr_sd = ifelse(length(nr) > 1, sd(nr), 0),
        nc_mean = mean(nc),
        nc_sd = ifelse(length(nc) > 1, sd(nc), 0),
        Qr = first(Qr),
        Qc = first(Qc)
    )
```

:::{.panel-tabset}
#### Parcours du clustering
```{r}
parcours_tree <- extract_clustering_tree(list_collection)
dendro_parcours_tree <- phylogram::as.dendrogram(parcours_tree)
p <- ggdendrogram(dendro_parcours_tree)
plotly::ggplotly(p)
```

#### Réseaux par cluster
```{r}
#| output: asis
for (i in seq_len(length(unlisted_best_partition))) {
    print(knitr::kable(
        x = unlisted_best_partition[[i]][["net_id"]],
        col.names = "Réseaux",
        caption = paste0("Réseaux membre du ", i, "e cluster")
    ))
    cat("\n")
}
```

#### Analyse des tailles de cluster

```{r}
filtered_summarized_graph_size_df <- summarized_graph_size_df
ggplot(filtered_summarized_graph_size_df) +
    aes(x = Qc, y = Qr, color = collection_id, label = M) +
    geom_point() + # , position = position_jitter(h = 0.15, w = 0.05)
    geom_text_repel() +
    xlab(TeX("$Q_c$")) +
    ylab(TeX("$Q_r$")) +
    coord_fixed()

plot_ly(x = filtered_summarized_graph_size_df$Qc, 
    y = filtered_summarized_graph_size_df$Qr, 
    z = filtered_summarized_graph_size_df$M,
    type = "scatter3d", mode = "markers",
    color = filtered_summarized_graph_size_df$collection_id)  %>% 
    layout(scene = list(xaxis = list(title = "Q_c"), 
        yaxis = list(title = "Q_r")))
```

#### Analyse des tailles de réseaux

```{r}
ggplot(filtered_summarized_graph_size_df) +
    aes(x = nc_mean, y = nr_mean, color = collection_id) +
    geom_point(size = 4) + # , position = position_jitter(h = 0.15, w = 0.05)
    geom_ellipse(aes(
        x0 = nc_mean, y0 = nr_mean,
        a = 0.5 * nc_sd,
        b = 0.5 * nr_sd,
        fill = collection_id, angle = 0
    ), alpha = 0.1) +
    geom_text_repel(aes(label = collection_id)) +
    xlab(TeX("$n_c$")) +
    ylab(TeX("$n_r$")) +
    coord_fixed() +
    ggtitle("Distribution des tailles moyennes et écart-types")

max_nr <- 200
max_nc <- 200

ggplot(filtered_summarized_graph_size_df %>% filter(nr_mean <= max_nr, nc_mean <= max_nc)) +
    aes(x = nc_mean, y = nr_mean, color = collection_id) +
    geom_point(size = 4) + # , position = position_jitter(h = 0.15, w = 0.05)
    geom_ellipse(aes(
        x0 = nc_mean, y0 = nr_mean,
        a = 0.5 * nc_sd,
        b = 0.5 * nr_sd,
        fill = collection_id, angle = 0
    ), alpha = 0.1) +
    geom_text_repel(aes(label = collection_id)) +
    xlab(TeX("$n_c$")) +
    ylab(TeX("$n_r$")) +
    coord_fixed() +
    ggtitle(TeX(paste0("Distribution des tailles moyennes et écart-types,", "$n_r <=", max_nr, "$", " et ", "$n_c <=", max_nc, "$")))

filtered_graph_size <- graph_size_df
ggplot(filtered_graph_size) +
    aes(x = collection_id, y = nr, fill = collection_id) +
    geom_boxplot()
ggplot(filtered_graph_size) +
    aes(x = collection_id, y = nc, fill = collection_id) +
    geom_boxplot()

ggplot(filtered_graph_size) +
    aes(x = collection_id, y = nr + nc, fill = collection_id) +
    geom_boxplot()

ggplot(filtered_graph_size) +
    aes(x = collection_id, y = nr * nc, fill = collection_id) +
    geom_boxplot()
```

#### Noeuds par clusters


:::

#### Structure des collections

```{r}
#| output: asis
for (i in seq_len(length(unlisted_best_partition))) {
    cat("\n\n##### Structure collection ", i, "\n")
    current_col <- unlisted_best_partition[[i]]$clone()
    current_col$net_id <- current_col$net_id %>%
        stringr::str_trunc(width = 20L)
    try({
        p <- plot(
            current_col,
            type = "meso",
            values = T, mixture = T
        ) +
            ggtitle(paste0("Structure\ncollection ", i))
        print(p)
    })
    cat("\n")
}

```