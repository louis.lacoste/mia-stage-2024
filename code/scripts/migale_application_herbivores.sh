#!/usr/bin/env bash
#$ -V
#$ -cwd
#$ -N herbivores_app
#$ -m besa
#$ -t 1:4
#$ -q long.q
#$ -pe thread 64
#$ -M louis.lacoste+migale@agroparistech.fr
#$ -o logs/$JOB_NAME.$TASK_ID
#$ -e logs/$JOB_NAME.$TASK_ID

# Creating log directory if it doesn't exists
BASE_DIR="/home/$USER/work/mia-stage-2024"
LOG_DIR=$(echo "$BASE_DIR/logs")

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p $LOG_DIR
fi

# Constant data
MODELARRAY=("iid" "pi" "rho" "pirho")

MODEL=${MODELARRAY[$(($((SGE_TASK_ID - 1)) % 4))]}

# Finding directory
APPLICATIONS_DIR=$(echo "$BASE_DIR/code/applications")

echo $APPLICATIONS_DIR

Rscript "${APPLICATIONS_DIR}/herbivores/02_herbivores_clustering.R" --model $MODEL
