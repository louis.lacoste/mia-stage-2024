#!/usr/bin/env bash
#$ -V
#$ -cwd
#$ -N NA_robustness_array
#$ -m besa
#$ -t 1:2
#$ -q short.q
#$ -pe thread 64
#$ -M louis.lacoste+migale@agroparistech.fr
#$ -o logs/$JOB_NAME.$TASK_ID
#$ -e logs/$JOB_NAME.$TASK_ID

# Creating log directory if it doesn't exists

# Constant data
STRUCTA=("nested" "modular")
SAMPLINGA=("uniform" "row" "col" "rowcol")

STRUCT=${STRUCTA[$(($((SGE_TASK_ID - 1)) % 2))]}

BASE_DIR="/home/$USER/work/mia-stage-2024/"
LOG_DIR=$(echo "$BASE_DIR/logs")

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p $LOG_DIR
fi

# Finding simulations directory
SIMULATIONS_DIR=$(echo "$BASE_DIR/code/simulations")

echo $SIMULATIONS_DIR

# Parsing sge array id

Rscript "${SIMULATIONS_DIR}/simulations_NA_robustness.R" --struct $STRUCT
