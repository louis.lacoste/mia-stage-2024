---
format:
  html:
    embed-resources: true
title: Clustering avec `colSBM` des données Baldock et al.
execute: 
  echo: false
  warning: false
---

# Analyse

```{r}
library(colSBM)
library(here)
library(stringr)
library(tidyr)
library(dplyr)
library(aricode)
library(reshape2)

library(ggplot2)
```

```{r}
root_app_folder <- file.path(here(), "code", "applications")
source(file.path(root_app_folder, "utils.R"))
data_folder <- file.path(here(), "code", "results", "applications", "sub-dore", "baldock")
files_vec <- get_recent_files(data_folder, n = 16L)
files_vec <- identify_models(files_vec, pattern = "(iid|pirho|pi|rho)_seed_[0-9]{1,4}")
names(files_vec) <- names(files_vec) %>% str_replace_all(pattern = "_", "~")
list_clustering <- files_vec
# list_collection <- readRDS(file.path(data_folder, "dore_collection_iid_24-05-24_18-07-50.Rds"))
```

## Analyse par modèle

Les clustering donne le critère suivant :
```{r}
vec_bicl <- sapply(list_clustering, function(clustering) {
    list_collection <- readRDS(clustering)
    unlisted_best_partition <- extract_best_partition(list_collection)
    if (is.list(unlisted_best_partition)) {
        BICL <- sum(sapply(
            unlisted_best_partition,
            function(col) col[["BICL"]]
        ))
    } else {
        BICL <- unlisted_best_partition[["BICL"]]
    }
    BICL
})

names(vec_bicl) <- names(files_vec)

col_order <- order(vec_bicl, decreasing = TRUE)
vec_bicl <- sort(vec_bicl, decreasing = TRUE)

list_clustering <- list_clustering[col_order]

knitr::kable(vec_bicl,
    caption = "BICL par modèle", col.names = "$BICL$",
    row.names = TRUE
)
```


```{r}
bicl_df <- do.call(
    "rbind",
    lapply(seq_along(list_clustering), function(idx) {
        partition <- readRDS(list_clustering[[idx]])
        unlisted_best_partition <- extract_best_partition(partition)
        if (!is.list(unlisted_best_partition)) {
            bicl_df <- get_details_bicl(unlisted_best_partition) %>%
                mutate(clustering = names(list_clustering)[[idx]])
        } else {
            bicl_df <- do.call("rbind", lapply(
                seq_len(length(unlisted_best_partition)),
                function(idx) {
                    get_details_bicl(unlisted_best_partition[[idx]]) %>%
                        mutate(col_id = idx)
                }
            )) %>%
                select(-col_id) %>%
                summarize_all(sum, na.rm = TRUE) %>%
                mutate(clustering = names(list_clustering)[[idx]])
        }
        bicl_df <- bicl_df %>% relocate(clustering)
        bicl_df
    })
)
knitr::kable(bicl_df, caption = "Détails des pénalités et du BIC-L", row.names = FALSE)

```

:::{.panel-tabset}

```{r write_tabs}
#| warning: false
#| output: asis

# Generate content for each model using knit_expand
for (clustering_idx in seq_len(length(list_clustering))) {
    clustering <- list_clustering[clustering_idx]
    model <- names(clustering)
    expanded_content <- knitr::knit_expand(file = file.path(root_app_folder, "base_analysis.qmd"), clustering = clustering, model = model)
    res <- knitr::knit_child(text = expanded_content, quiet = TRUE)
    cat(res, sep = "\n")
    cat("\n")
}
```

:::

## Comparaison des modèles

### Table des clustering

```{r}
clustering_df <- sapply(seq_len(length(list_clustering)), function(idx) {
    clust <- readRDS(list_clustering[[idx]])
    extract_clustering(clust)
})

clust_to_compare_order <- crossing(
    f = seq_len(ncol(clustering_df)),
    s = seq_len(ncol(clustering_df))
) %>%
    filter(!(f == s) & f < s)

clustering_partitions_df <- do.call("rbind", lapply(seq_len(length(list_clustering)), function(idx) {
    clust <- readRDS(list_clustering[[idx]])
    clust_vec <- extract_clustering(clust)
    net_id <- names(clust_vec)
    clust_vec <- unname(clust_vec)
    clust_idx <- idx
    data.frame(net_id = net_id, cluster = clust_vec, clustering_id = clust_idx)
}))

clustering_partitions_df <- clustering_partitions_df %>%
    # mutate(net_id = str_trunc(net_id, width = 20L)) %>%
    mutate_all(as.factor)

```

```{r ARI}
ari_dist_matrix <- outer(
    X = seq_len(ncol(clustering_df)),
    Y = seq_len(ncol(clustering_df)),
    Vectorize(function(x, y) {
        f_clust <- clustering_df[, x]
        s_clust <- clustering_df[, y]
        ARI(f_clust, s_clust)
    })
)
ari_plot_data <- ari_dist_matrix %>%
    melt() %>%
    mutate(Var1 = names(list_clustering)[Var1], Var2 = names(list_clustering)[Var2])
ggplot(ari_plot_data) +
    aes(x = Var1, y = Var2) +
    geom_raster(aes(fill = value)) +
    scale_fill_gradient(low = "white", high = "red", limits = c(0, 1)) +
    geom_text(aes(label = round(value, digits = 2))) +
    labs(
        x = "id clusterings",
        y = "id clusterings", title = "ARI entre les clusterings"
    ) +
    theme_bw()
```

<details>
<summary> Tables de cooccurrence </summary>

```{r table cooccurrence}
list_tables_coocc <- lapply(seq_len(nrow(clust_to_compare_order)), function(row_id) {
    f <- clust_to_compare_order[[row_id, 1]]
    s <- clust_to_compare_order[[row_id, 2]]
    table(clustering_df[, f], clustering_df[, s])
})
```

```{r function plot tables}
plot_table <- function(data, f = NULL, s = NULL) {
    melted_data <- data %>%
        melt() %>%
        mutate(
            Var1 = as.factor(Var1),
            Var2 = as.factor(Var2)
        )

    ggplot(melted_data) +
        aes(y = Var1, x = Var2) +
        geom_raster(aes(fill = value)) +
        scale_fill_gradient(low = "white", high = "red", limits = c(0, NA)) +
        scale_x_discrete() +
        geom_text(data = subset(melted_data, value != 0), aes(label = value)) +
        labs(
            x = paste0("numéros clusters pour clustering : ", s),
            y = paste0("numéros clusters pour clustering : ", f), title = paste0("Co-occurence entre les clusterings ", f, " et ", s)
        ) +
        theme_bw()
}
```

```{r tables coocc plot}
#| output: asis

for (id in seq_len(length(list_tables_coocc))) {
    print(plot_table(list_tables_coocc[[id]],
        f = names(list_clustering)[clust_to_compare_order[[id, 1]]],
        s = names(list_clustering)[clust_to_compare_order[[id, 2]]]
    ))
}
```
</details>