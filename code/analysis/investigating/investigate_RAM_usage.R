library(profvis)
library(here)

special <- "preschedule_10net"
backend <- "parallel"

data_folder <- file.path(here(), "code", "results", "investigating", "ram_usage")

if (!(dir.exists(data_folder))) {
    dir.create(data_folder)
}

filename <- paste0(
    "prof_vignette_", backend, "_", special, "_",
    format(Sys.time(), "%d-%M-%Y_%H-%m-%S"), ".Rdata"
)

data_file <- file.path(data_folder, filename)

gcinfo(verbose = TRUE)
profvis({
    ## ----setup---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    library(colSBM)
    library(patchwork)
    library(parallel)
    data("dorebipartite")


    ## ----warning=FALSE-------------------------------------------------------------------------------------------------------------------------------------------------------------------
    set.seed(1234, "L'Ecuyer-CMRG")

    ## ----results=FALSE, warning=FALSE----------------------------------------------------------------------------------------------------------------------------------------------------
    net_clust <- clusterize_bipartite_networks(
        netlist = c(dorebipartite[1L:10L]), # A list of networks
        colsbm_model = "iid", # The name of the model
        nb_run = 1L, # Number of runs of the algorithm
        global_opts = list(
            verbosity = 0L,
            plot_details = 0L,
            nb_cores = 2L,
            backend = backend,
            Q1_max = 9L,
            Q2_max = 9L
        ) # Max number of clusters
    )
},
prof_output = data_file
)
