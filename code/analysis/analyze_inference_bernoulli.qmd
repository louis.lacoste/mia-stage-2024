---
title: "Analysis inference for Bernoulli distribution 3run"
output: html
execute: 
    cache: true
    echo: false
    warning: false
---

```{r knitropts, echo = FALSE}
knitr::opts_chunk$set(fig.width = 12L, fig.height = 10L)
```

```{r imports, echo = FALSE, include=FALSE}
suppressPackageStartupMessages(library(ggplot2))
suppressPackageStartupMessages(library(tidyverse))
suppressPackageStartupMessages(library(latex2exp))
suppressPackageStartupMessages(library(here))
library(latex2exp)
library(flextable)
loadNamespace(package = "patchwork")
loadNamespace("knitr")
loadNamespace("kableExtra")
```

```{r data, echo = FALSE}
#  Loading data
data_folder <- file.path(here(), "code", "results", "simulations", "inference", "bernoulli")
# files_list <- list.files(data_folder, pattern = "^NA_robustness_19-04-2024")
data <- readRDS(file.path(data_folder, "bernoulli_inference_06-05-2024_17-21-48_1-972.Rds"))
# data <- do.call("rbind", lapply(list.files(file.path(data_folder, "tmp02-05-2024_11-15-20"), pattern = "^conditions*", full.names = TRUE), function(filepath) readRDS(filepath)))

```

```{r data-transfo, echo = FALSE}
av_data <- data %>%
    group_by(
        epsilon_alpha, pi1.1, pi1.2, pi1.3, pi1.4,
        rho2.1, rho2.2, rho2.3, rho2.4, repetition
    ) %>%
    mutate(
        pirho_over_sep = pirho_BICL > sep_BICL,
        pirho_over_iid = pirho_BICL > iid_BICL,
        pirho_over_pi = pirho_BICL > pi_BICL,
        pirho_over_rho = pirho_BICL > rho_BICL,
        pirho_Q1_under_4 = pirho_Q1 < 4,
        pirho_Q1_equal_4 = pirho_Q1 == 4,
        pirho_Q1_over_4 = pirho_Q1 > 4,
        pirho_Q2_under_4 = pirho_Q2 < 4,
        pirho_Q2_equal_4 = pirho_Q2 == 4,
        pirho_Q2_over_4 = pirho_Q2 > 4
    ) %>%
    ungroup() %>%
    group_by(epsilon_alpha) %>%
    select(contains("pirho_")) %>%
    summarise_all(list(mean = mean, sd = sd)) %>%
    select(-((contains(c("over", "under", "equal")) & contains("sd")) | contains(c("BICL_mean", "_Q2_mean", "Q1_mean", "BICL_sd", "_Q2_sd", "Q1_sd"))))
```

```{r data_print}
print_data <- av_data %>%
    group_by(epsilon_alpha) %>%
    round(2) %>%
    mutate(
        #  Model comparison
        pirho_over_sep = pirho_over_sep_mean,
        pirho_over_iid = pirho_over_iid_mean,
        pirho_over_pi = pirho_over_pi_mean,
        pirho_over_rho = pirho_over_rho_mean,

        # Estimation of Q
        pirho_Q1_under_4 = pirho_Q1_under_4_mean,
        pirho_Q1_equal_4 = pirho_Q1_equal_4_mean,
        pirho_Q1_over_4 = pirho_Q1_over_4_mean,
        pirho_Q2_under_4 = pirho_Q2_under_4_mean,
        pirho_Q2_equal_4 = pirho_Q2_equal_4_mean,
        pirho_Q2_over_4 = pirho_Q2_over_4_mean,


        #  Grouping accuracy
        pirho_mean_row_ARI = paste(pirho_mean_row_ARI_mean,
            pirho_mean_row_ARI_sd,
            sep = "\\pm"
        ),
        pirho_mean_col_ARI = paste(pirho_mean_col_ARI_mean,
            pirho_mean_col_ARI_sd,
            sep = "\\pm"
        ),
        pirho_double_row_ARI = paste(pirho_double_row_ARI_mean,
            pirho_double_row_ARI_sd,
            sep = "\\pm"
        ),
        pirho_double_col_ARI = paste(pirho_double_col_ARI_mean,
            pirho_double_col_ARI_sd,
            sep = "\\pm"
        ),
        .keep = "none"
    )
```

```{r table, echo = FALSE}
# knitr::kable(print_data, col.names = col_keys_latex, escape = TRUE)
#  %>%
#     kableExtra::add_header_above(c(
#         " " = 1,
#         "Model comparison" = 4,
#         "Estimation of Q" = 6, "Grouping accuracy" = 4
#     ),escape = TRUE)
```

```{r}
col_keys <- c(
    "\\epsilon_\\alpha",
    "\\pi\\rho\\text{ pref. to }sep",
    "\\pi\\rho\\text{ pref. to }iid",
    "\\pi\\rho\\text{ pref. to }\\pi",
    "\\pi\\rho\\text{ pref. to }\\rho",
    "1_{\\widehat{Q_1} < 4}",
    "1_{\\widehat{Q_1} = 4}",
    "1_{\\widehat{Q_1} > 4}",
    "1_{\\widehat{Q_2} < 4}",
    "1_{\\widehat{Q_2} = 4}",
    "1_{\\widehat{Q_2} > 4}",
    "\\overline{ARI}^{d=1}",
    "\\overline{ARI}^{d=2}",
    "ARI^{d=1}_{1,2}",
    "ARI^{d=2}_{1,2}"
)

ft_1 <- flextable(print_data) |>
    set_table_properties(
        opts_html = list(
            scroll = list(freeze_first_column = TRUE)
        )
    ) |>
    compose(j = "pirho_mean_row_ARI", value = as_paragraph(as_equation(pirho_mean_row_ARI)))
ft_1 <- compose(ft_1, j = "pirho_mean_col_ARI", value = as_paragraph(as_equation(pirho_mean_col_ARI)))
ft_1 <- compose(ft_1, j = "pirho_double_row_ARI", value = as_paragraph(as_equation(pirho_double_row_ARI)))
ft_1 <- compose(ft_1, j = "pirho_double_col_ARI", value = as_paragraph(as_equation(pirho_double_col_ARI)))
ft_1 <- ft_1 |> mk_par(
    i = 1, part = "header",
    value = as_paragraph(
        as_equation(col_keys, width = .1, height = .2)
    )
)
theme_zebra(ft_1) |>
    hline(i = 1, border = fp_border_default(), part = "header") |>
    vline(j = 1, border = fp_border_default(), part = "all") |>
    bg(j = 1, bg = "#DDDDDD", part = "all") |>
    bg(i = 1, bg = "#DDDDDD", part = "header")
```