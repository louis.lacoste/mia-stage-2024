#!/usr/bin/env bash
#$ -V
#$ -cwd
#$ -N traveset
#$ -m besa
#$ -t 1:16
#$ -q short.q
#$ -pe thread 64
#$ -M louis.lacoste+migale@agroparistech.fr
#$ -o logs/$JOB_NAME.$TASK_ID
#$ -e logs/$JOB_NAME.$TASK_ID

# Creating log directory if it doesn't exists
BASE_DIR="/home/$USER/work/mia-stage-2024"
LOG_DIR=$(echo "$BASE_DIR/logs")

if [ ! -d "$LOG_DIR" ]; then
    mkdir -p $LOG_DIR
fi

# Constant data
MODELARRAY=("iid" "pi" "rho" "pirho")
ID=$((SGE_TASK_ID - 1))
MODEL=${MODELARRAY[$((ID % 4))]}

SEED=$(($((ID + $((ID / 4)))) % 4))


# Finding directory
APPLICATIONS_DIR=$(echo "$BASE_DIR/code/applications")

echo $APPLICATIONS_DIR

Rscript "${APPLICATIONS_DIR}/sub-dore/traveset/01_traveset_clusterize.R" --model $MODEL --seed $SEED
