set.seed(0)

# Should not be used standalone !
base_alpha <- matrix(rep(0.3, 9L), nrow = 3L)
pi <- c(0.3, 0.2, 0.5)
rho <- c(0.55, 0.15, 0.3)
M <- 40L
nr <- c(rep(30L, M / 2L), rep(95L, M / 2L))
nc <- c(rep(40L, M / 2L), rep(70L, M / 2L))

message("Génération de la collection synthétique avec la seed ", seed)

generate_synth_col <- function(eps) {
    as_alpha <- base_alpha + matrix(
        c(
            eps, -eps / 2L, -eps / 2L,
            -eps / 2L, eps, -eps / 2L,
            -eps / 2L, -eps / 2L, eps
        ),
        nrow = 3L
    )

    cp_alpha <- base_alpha + matrix(
        c(
            3L * eps / 2L, eps, eps / 2L,
            eps, eps / 2L, 0L,
            eps / 2L, 0L, -eps / 2L
        ),
        nrow = 3L
    )

    dis_alpha <- base_alpha + matrix(
        c(
            -eps / 2L, eps, eps,
            eps, -eps / 2L, eps,
            eps, eps, -eps / 2L
        ),
        nrow = 3L
    )

    collection <- c(
        generate_bipartite_collection(
            nr = nr, nc = nc,
            pi = pi, rho = rho,
            alpha = as_alpha, M = M
        ),
        generate_bipartite_collection(
            nr = nr, nc = nc,
            pi = pi, rho = rho,
            alpha = cp_alpha, M = M
        ),
        generate_bipartite_collection(
            nr = nr, nc = nc,
            pi = pi, rho = rho,
            alpha = dis_alpha, M = M
        )
    )
    names(collection) <- c(
        rep("assortative_small", M / 2), rep("assortative", M / 2),
        rep("core_periphery_small", M / 2), rep("core_periphery", M / 2),
        rep("disassortative_small", M / 2), rep("disassortative", M / 2)
    )
    collection
}
